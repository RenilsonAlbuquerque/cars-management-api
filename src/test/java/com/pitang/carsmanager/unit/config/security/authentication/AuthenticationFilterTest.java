package com.pitang.carsmanager.unit.config.security.authentication;

import com.pitang.carsmanager.config.security.authentication.ApiAuthenticationManager;
import com.pitang.carsmanager.config.security.authentication.AuthenticationFilter;
import com.pitang.carsmanager.config.security.authentication.mapper.LoginMapper;
import com.pitang.carsmanager.config.security.authentication.model.AuthenticationContext;
import com.pitang.carsmanager.config.security.jwt.JwtTokenProvider;
import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.entrypoint.rest.commons.dto.LoginDTO;
import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;


import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

class AuthenticationFilterTest {

    JwtTokenProvider jwtTokenProvider;

    AuthenticationManager authenticationManager;

    AuthenticationFilter authenticationFilter;

    public AuthenticationFilterTest() {
        this.jwtTokenProvider = mock(JwtTokenProvider.class);
        this.authenticationManager = mock(ApiAuthenticationManager.class);
        this.authenticationFilter = spy(new AuthenticationFilter("/singin",authenticationManager,
                jwtTokenProvider));
    }

    @Test
    void shouldCreateAuthenticationWithSuccess() {

        final var request = mock(HttpServletRequest.class);

        try (MockedStatic<LoginMapper> utilities = Mockito.mockStatic(LoginMapper.class)) {
            utilities.when(() -> LoginMapper.getLoginDtoFromServletRequest(any(HttpServletRequest.class)))
                    .thenReturn(new UsernamePasswordAuthenticationToken("", ""));

            final var authenticationStub = new AuthenticationContext(1L,"name");
            when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
                    .thenReturn(authenticationStub);

            final var output = this.authenticationFilter.attemptAuthentication(request,null);
            assertNotNull(output);
        }
    }

    @Test
    void whenAuthenticatorThrowsBusinessExceptionShouldThrowBadCredentialsException() throws IOException {

        final var request = mock(HttpServletRequest.class);

        try (MockedStatic<LoginMapper> utilities = Mockito.mockStatic(LoginMapper.class)) {
            utilities.when(() -> LoginMapper.getLoginDtoFromServletRequest(any(HttpServletRequest.class)))
                    .thenReturn(new UsernamePasswordAuthenticationToken("",""));

            when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
                    .thenThrow(new BusinessException(1,"message"));

            assertThrows(BadCredentialsException.class, () ->
                    this.authenticationFilter.attemptAuthentication(request,null));
        }


    }

}
