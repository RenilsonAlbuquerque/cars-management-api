package com.pitang.carsmanager.unit.config.security.authentication;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.config.security.authentication.ApiAuthenticationManager;
import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.domain.user.model.UserModel;
import com.pitang.carsmanager.domain.user.usecase.find.FindUserByLoginUsecase;
import com.pitang.carsmanager.fixture.user.model.UserModelTemplates;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class ApiAuthenticationManagerTest {

    @Mock
    FindUserByLoginUsecase findUserByLogin;

    @InjectMocks
    ApiAuthenticationManager authenticationManager;

    @BeforeAll
    public static void configureFixture() {
        FixtureFactoryLoader.loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void shouldCreateAuthenticationContext() {
        final var passwordStub = "password";
        final var authInput =  new UsernamePasswordAuthenticationToken("username", passwordStub);

        final UserModel userStub = Fixture.from(UserModel.class)
                .gimme(UserModelTemplates.COMPLETE_USER_MODEL);
        userStub.setPassword(passwordStub);

        Mockito.when(findUserByLogin.execute(anyString()))
                .thenReturn(Optional.of(userStub));

        final var output = this.authenticationManager.authenticate(authInput);
        assertNotNull(output);
    }

    @Test
    void givenNonexistentLoginShouldThrowException() {
        final var authInput =  new UsernamePasswordAuthenticationToken("username", "password");

        Mockito.when(findUserByLogin.execute(anyString()))
                .thenReturn(Optional.empty());

        assertThrows(BusinessException.class, () -> this.authenticationManager.authenticate(authInput));
    }

    @Test
    void givenNInvalidPasswordShouldThrowException() {
        final var authInput =  new UsernamePasswordAuthenticationToken("username", "pass");

        final UserModel userStub = Fixture.from(UserModel.class)
                .gimme(UserModelTemplates.COMPLETE_USER_MODEL);
        userStub.setPassword("Pass2");

        Mockito.when(findUserByLogin.execute(anyString()))
                .thenReturn(Optional.of(userStub));

        assertThrows(BusinessException.class, () -> this.authenticationManager.authenticate(authInput));
    }

}
