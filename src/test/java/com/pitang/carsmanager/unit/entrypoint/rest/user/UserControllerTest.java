package com.pitang.carsmanager.unit.entrypoint.rest.user;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.model.UserModel;
import com.pitang.carsmanager.domain.user.usecase.create.CreateUserUsecase;
import com.pitang.carsmanager.domain.user.usecase.find.FindAllUsersUsecase;
import com.pitang.carsmanager.entrypoint.rest.user.UserController;
import com.pitang.carsmanager.entrypoint.rest.user.dto.CreateUserDto;
import com.pitang.carsmanager.entrypoint.rest.user.mapper.CreateUserRestMapper;
import com.pitang.carsmanager.fixture.user.dto.CreateUserDtoTemplates;
import com.pitang.carsmanager.fixture.user.model.UserModelTemplates;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    CreateUserRestMapper createUserRestMapper;

    @Mock
    CreateUserUsecase createUserUsecase;

    @Mock
    FindAllUsersUsecase findAllUsersUsecase;

    @InjectMocks
    UserController userController;

    @BeforeAll
    public static void configureFixture() {

        FixtureFactoryLoader.loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void shouldCreateUserWithSuccess() {

        final CreateUserDto createUserInput = Fixture.from(CreateUserDto.class)
                .gimme(CreateUserDtoTemplates.COMPLETE_CREATE_USER_DTO);

        final var createUserModelStub = CreateUserModel.builder()
                .firstName("test")
                .build();

        Mockito.when(createUserRestMapper.dtoToDomainModel(createUserInput))
                .thenReturn(createUserModelStub);
        Mockito.when(createUserUsecase.execute(any(CreateUserModel.class)))
                .thenReturn(createUserModelStub);

        final var output = this.userController.createUser(createUserInput);
        assertEquals(createUserInput.getFirstName(), output.getFirstName());
    }

    @Test
    void shouldListUsersWithSuccess() {

        final CreateUserDto userDtoStub = Fixture.from(CreateUserDto.class)
                .gimme(CreateUserDtoTemplates.COMPLETE_CREATE_USER_DTO);

        final UserModel usersModelStub = Fixture.from(UserModel.class)
                        .gimme(UserModelTemplates.COMPLETE_USER_MODEL);

        Mockito.when(createUserRestMapper.domainToDto(any()))
                .thenReturn(userDtoStub);

        Mockito.when(findAllUsersUsecase.execute())
                .thenReturn(List.of(usersModelStub));

        final var output = this.userController.listAllUsers();
        assertEquals(List.of(userDtoStub).size(), output.size());
    }
}
