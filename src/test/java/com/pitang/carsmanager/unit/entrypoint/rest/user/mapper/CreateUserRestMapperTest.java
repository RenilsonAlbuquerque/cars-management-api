package com.pitang.carsmanager.unit.entrypoint.rest.user.mapper;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.entrypoint.rest.user.dto.CreateUserDto;
import com.pitang.carsmanager.entrypoint.rest.user.mapper.CreateUserRestMapper;
import com.pitang.carsmanager.fixture.user.dto.CreateUserDtoTemplates;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CreateUserRestMapperTest {


    CreateUserRestMapper createUserRestMapper = new CreateUserRestMapper();

    @BeforeAll
    public static void configureFixture() {

        FixtureFactoryLoader.loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void shouldMapUserWithSuccess() {
        final CreateUserDto createUserInput = Fixture.from(CreateUserDto.class)
                .gimme(CreateUserDtoTemplates.COMPLETE_CREATE_USER_DTO);

        final var output = createUserRestMapper.dtoToDomainModel(createUserInput);
        assertEquals(createUserInput.getFirstName(), output.getFirstName());
        assertEquals(createUserInput.getLastName(), output.getLastName());
        assertEquals(createUserInput.getLogin(), output.getLogin());
        assertEquals(createUserInput.getEmail(), output.getEmail());
        assertEquals(createUserInput.getPassword(), output.getPassword());
        assertEquals(createUserInput.getPhone(), output.getPhone());
    }
}
