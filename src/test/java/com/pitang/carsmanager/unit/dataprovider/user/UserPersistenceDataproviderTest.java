package com.pitang.carsmanager.unit.dataprovider.user;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.dataproviders.user.UserPersistenceDataprovider;
import com.pitang.carsmanager.dataproviders.user.entity.UserEntity;
import com.pitang.carsmanager.dataproviders.user.mapper.UserRepositoryMapper;
import com.pitang.carsmanager.dataproviders.user.repository.UserRepositoryH2;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.model.UserModel;
import com.pitang.carsmanager.fixture.user.entity.UserEntityTemplates;
import com.pitang.carsmanager.fixture.user.model.CreateUserModelTemplates;
import com.pitang.carsmanager.fixture.user.model.UserModelTemplates;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class UserPersistenceDataproviderTest {

    @Mock
    UserRepositoryMapper mapper;

    @Mock
    UserRepositoryH2 userRepositoryH2;

    @InjectMocks
    UserPersistenceDataprovider userPersistenceDataprovider;

    @BeforeAll
    public static void configureFixture() {
        FixtureFactoryLoader
                .loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void shouldMapEntityAndSaveToRepository() {

        final CreateUserModel createUserInput = Fixture.from(CreateUserModel.class)
                .gimme(CreateUserModelTemplates.COMPLETE_CREATE_USER_MODEL);

        final UserEntity userEntityStub = Fixture.from(UserEntity.class)
                .gimme(UserEntityTemplates.COMPLETE_USER_ENTITY);

        Mockito.when(mapper.domainModelToEntity(createUserInput))
                .thenReturn(userEntityStub);

        Mockito.when(userRepositoryH2.save(any(UserEntity.class)))
                .thenReturn(userEntityStub);

        final var output = this.userPersistenceDataprovider.save(createUserInput);
        assertNotNull(output);
    }

    @ParameterizedTest
    @DisplayName("Should find by email")
    @MethodSource("searchArguments")
    void givenValidEmailShouldRetrieveUser(Optional<UserEntity> parameter) {

        final String emailInput = "test";

        Mockito.when(userRepositoryH2.findByEmail(emailInput))
                .thenReturn(parameter);

        parameter.ifPresent(userEntity -> Mockito.when(mapper.entityToDomainModel(userEntity))
                .thenReturn(Fixture.from(UserModel.class)
                        .gimme(UserModelTemplates.COMPLETE_USER_MODEL)));

        final var output = this.userPersistenceDataprovider.findByEmail(emailInput);
        assertEquals(parameter.isPresent(),output.isPresent());
    }

    @ParameterizedTest
    @DisplayName("Should find by login")
    @MethodSource("searchArguments")
    void givenValidLoginShouldRetrieveUser(Optional<UserEntity> parameter) {

        final String emailInput = "test";

        Mockito.when(userRepositoryH2.findByLogin(emailInput))
                .thenReturn(parameter);

        parameter.ifPresent(userEntity -> Mockito.when(mapper.entityToDomainModel(userEntity))
                .thenReturn(Fixture.from(UserModel.class)
                        .gimme(UserModelTemplates.COMPLETE_USER_MODEL)));

        final var output = this.userPersistenceDataprovider.findByLogin(emailInput);
        assertEquals(parameter.isPresent(),output.isPresent());
    }

    static Stream<Arguments> searchArguments() {
        return Stream.of(
                arguments(Optional.of(new UserEntity())),
                arguments(Optional.empty())
        );
    }


    @Test
    void shouldListAllUsersFromRepositoryWithSuccess() {

        final UserModel userStub = Fixture.from(UserModel.class)
                .gimme(UserModelTemplates.COMPLETE_USER_MODEL);

        final UserEntity userEntityStub = Fixture.from(UserEntity.class)
                .gimme(UserEntityTemplates.COMPLETE_USER_ENTITY);

        final var usersListStub = List.of(userEntityStub);
        Mockito.when(mapper.entityToDomainModel(userEntityStub))
                .thenReturn(userStub);

        Mockito.when(userRepositoryH2.findAll())
                .thenReturn(List.of(userEntityStub));

        final var output = this.userPersistenceDataprovider.findAll();
        assertEquals(usersListStub.size(),output.size());
    }
}
