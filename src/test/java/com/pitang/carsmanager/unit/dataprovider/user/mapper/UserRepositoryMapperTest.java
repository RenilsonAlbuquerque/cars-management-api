package com.pitang.carsmanager.unit.dataprovider.user.mapper;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.dataproviders.user.entity.UserEntity;
import com.pitang.carsmanager.dataproviders.user.mapper.UserRepositoryMapper;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.fixture.user.entity.UserEntityTemplates;
import com.pitang.carsmanager.fixture.user.model.CreateUserModelTemplates;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class UserRepositoryMapperTest {


    UserRepositoryMapper createUserRestMapper = new UserRepositoryMapper();

    @BeforeAll
    public static void configureFixture() {

        FixtureFactoryLoader.loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void shouldMapCreateUserModelToEntityWithSuccess() {
        final CreateUserModel createUserInput = Fixture.from(CreateUserModel.class)
                .gimme(CreateUserModelTemplates.COMPLETE_CREATE_USER_MODEL);

        final var output = createUserRestMapper.domainModelToEntity(createUserInput);
        assertEquals(createUserInput.getFirstName(), output.getFirstName());
        assertEquals(createUserInput.getLastName(), output.getLastName());
        assertEquals(createUserInput.getLogin(), output.getLogin());
        assertEquals(createUserInput.getEmail(), output.getEmail());
        assertEquals(createUserInput.getPassword(), output.getUserPassword());
        assertEquals(createUserInput.getPhone(), output.getPhone());
    }

    @Test
    void shouldMapUserEntityToModelWithSuccess() {
        final UserEntity createUserInput = Fixture.from(UserEntity.class)
                .gimme(UserEntityTemplates.COMPLETE_USER_ENTITY);

        final var output = createUserRestMapper.entityToDomainModel(createUserInput);
        assertEquals(createUserInput.getFirstName(), output.getFirstName());
        assertEquals(createUserInput.getLastName(), output.getLastName());
        assertEquals(createUserInput.getLogin(), output.getLogin());
        assertEquals(createUserInput.getEmail(), output.getEmail());
        assertEquals(createUserInput.getUserPassword(), output.getPassword());
        assertEquals(createUserInput.getPhone(), output.getPhone());
    }
}
