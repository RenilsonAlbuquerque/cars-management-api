package com.pitang.carsmanager.unit.domain.user.create;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.usecase.create.CreateUserUsecase;
import com.pitang.carsmanager.domain.user.usecase.create.validators.ICreateUserValidator;
import com.pitang.carsmanager.fixture.user.model.CreateUserModelTemplates;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class CreateUserUsecaseTest {


    List<ICreateUserValidator> validators;

    IUserPersistenceGateway userPersistenceGateway;

    CreateUserUsecase createUserUsecase;

    public CreateUserUsecaseTest() {
        userPersistenceGateway = mock(IUserPersistenceGateway.class);
        validators = new ArrayList<>();
        createUserUsecase = new CreateUserUsecase(validators,userPersistenceGateway);
    }

    @BeforeAll
    public static void configureFixture() {
        FixtureFactoryLoader.loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void shouldValidateAndCallGatewaySaveUserWithSuccess() {

        final CreateUserModel createUserInput = Fixture.from(CreateUserModel.class)
                .gimme(CreateUserModelTemplates.COMPLETE_CREATE_USER_MODEL);

        final var output = this.createUserUsecase.execute(createUserInput);
        assertNotNull(output);
        verify(this.userPersistenceGateway, times(1)).save(createUserInput);
    }

    @Test
    void shouldThrowExceptionOnValidation() {

        final CreateUserModel createUserInput = Fixture.from(CreateUserModel.class)
                .gimme(CreateUserModelTemplates.COMPLETE_CREATE_USER_MODEL);

        final var failureValidator = new ICreateUserValidator() {
            @Override
            public boolean validate(CreateUserModel createUserModel) {
                throw new BusinessException(1,"Error Code");
            }
        };
        this.validators.add(failureValidator);

        assertThrows(BusinessException.class, () -> this.createUserUsecase.execute(createUserInput));
        verify(this.userPersistenceGateway, times(0)).save(createUserInput);
    }
}
