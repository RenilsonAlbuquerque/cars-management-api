package com.pitang.carsmanager.unit.domain.user.create.validators;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.usecase.create.validators.MissingFieldsValidator;
import com.pitang.carsmanager.fixture.user.model.CreateUserModelTemplates;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class MissingFieldsValidatorTest {

    Validator validator;

    MissingFieldsValidator missingFieldsValidator;

    public MissingFieldsValidatorTest() {
        this.validator = Validation.buildDefaultValidatorFactory().getValidator();
        missingFieldsValidator = new MissingFieldsValidator(validator);
    }
    @BeforeAll
    public static void configureFixture() {
        FixtureFactoryLoader
                .loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void givenUserObjectWithMissingFieldsShouldThrowBusinessException() {

        final CreateUserModel createUserInput = new CreateUserModel();

        assertThrows(BusinessException.class, () -> this.missingFieldsValidator.validate(createUserInput));
    }

    @Test
    void givenValidCreateUserObjectShouldReturnTrue() {

        final CreateUserModel createUserInput = Fixture.from(CreateUserModel.class)
                .gimme(CreateUserModelTemplates.COMPLETE_CREATE_USER_MODEL);

        final var output = this.missingFieldsValidator.validate(createUserInput);
        assertTrue(output);
    }

}
