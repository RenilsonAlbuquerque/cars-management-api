package com.pitang.carsmanager.unit.domain.user.find;


import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.dataproviders.user.entity.UserEntity;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.UserModel;
import com.pitang.carsmanager.domain.user.usecase.find.FindUserByLoginUsecase;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@ExtendWith(MockitoExtension.class)
public class FindUserByLoginUsecaseTest {

    @Mock
    IUserPersistenceGateway persistenceGateway;

    @InjectMocks
    FindUserByLoginUsecase findUserByLogin;

    @BeforeAll
    public static void configureFixture() {
        FixtureFactoryLoader.loadTemplates("com.pitang.carsmanager.fixture");
    }

    @ParameterizedTest
    @DisplayName("Should find by login")
    @MethodSource("searchArguments")
    void givenValidEmailShouldRetrieveUser(Optional<UserModel> userModel) {

        final String loginInput = "test";


        Mockito.when(this.persistenceGateway.findByLogin(loginInput))
                .thenReturn(userModel);

        final var output = this.findUserByLogin.execute(loginInput);
        assertEquals(userModel.isPresent(),output.isPresent());
    }


    static Stream<Arguments> searchArguments() {
        return Stream.of(
                arguments(Optional.of(new UserEntity())),
                arguments(Optional.empty())
        );
    }
}
