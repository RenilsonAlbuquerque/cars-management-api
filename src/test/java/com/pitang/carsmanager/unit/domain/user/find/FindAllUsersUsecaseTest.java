package com.pitang.carsmanager.unit.domain.user.find;


import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.UserModel;
import com.pitang.carsmanager.domain.user.usecase.find.FindAllUsersUsecase;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class FindAllUsersUsecaseTest {

    @Mock
    IUserPersistenceGateway persistenceGateway;

    @InjectMocks
    FindAllUsersUsecase findAllUsersUsecase;

    @BeforeAll
    public static void configureFixture() {
        FixtureFactoryLoader.loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void shouldListAllUsers() {

        final var usersStub = List.of(new UserModel());
        Mockito.when(this.persistenceGateway.findAll())
                .thenReturn(usersStub);

        final var output = this.findAllUsersUsecase.execute();
        assertEquals(usersStub.size(),output.size());
    }
}
