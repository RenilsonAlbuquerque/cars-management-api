package com.pitang.carsmanager.unit.domain.user.create.validators;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.model.UserModel;
import com.pitang.carsmanager.domain.user.usecase.create.validators.EmailValidator;
import com.pitang.carsmanager.fixture.user.model.CreateUserModelTemplates;
import com.pitang.carsmanager.fixture.user.model.UserModelTemplates;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class EmailValidatorTest {

    @Mock
    IUserPersistenceGateway userPersistenceGateway;

    @InjectMocks
    EmailValidator emailValidator;

    @BeforeAll
    public static void configureFixture() {
        FixtureFactoryLoader.loadTemplates("com.pitang.carsmanager.fixture");
    }

    @Test
    void whenFoundExistingUserWithEmailShouldThrowBusinessException() {

        final CreateUserModel createUserInput = Fixture.from(CreateUserModel.class)
                .gimme(CreateUserModelTemplates.COMPLETE_CREATE_USER_MODEL);

        final UserModel userStub = Fixture.from(UserModel.class)
                .gimme(UserModelTemplates.COMPLETE_USER_MODEL);

        Mockito.when(this.userPersistenceGateway.findByEmail(createUserInput.getEmail()))
                .thenReturn(Optional.of(userStub));

        assertThrows(BusinessException.class, () ->this.emailValidator.validate(createUserInput));
    }

    @Test
    void whenNotFoundExistingUserWithEmailShouldReturnTrue() {

        final CreateUserModel createUserInput = Fixture.from(CreateUserModel.class)
                .gimme(CreateUserModelTemplates.COMPLETE_CREATE_USER_MODEL);


        Mockito.when(this.userPersistenceGateway.findByEmail(createUserInput.getEmail()))
                .thenReturn(Optional.empty());

        final var output = this.emailValidator.validate(createUserInput);
        assertTrue(output);
    }

}
