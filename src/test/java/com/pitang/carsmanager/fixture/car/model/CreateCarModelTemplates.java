package com.pitang.carsmanager.fixture.car.model;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.pitang.carsmanager.domain.user.model.CreateCarModel;

public class CreateCarModelTemplates implements TemplateLoader {

    public static final String COMPLETE_CREATE_CAR_MODEL = "complete create car model";

    @Override
    public void load() {
        Fixture.of(CreateCarModel.class).addTemplate(COMPLETE_CREATE_CAR_MODEL, new Rule() {
            {
                add("year", 1);
                add("licensePlate", "licensePlate");
                add("model", "model");
                add("color", "color");
            }
        });
    }
}
