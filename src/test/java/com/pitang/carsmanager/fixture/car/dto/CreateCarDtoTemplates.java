package com.pitang.carsmanager.fixture.car.dto;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.pitang.carsmanager.entrypoint.rest.user.dto.CreateCarDto;

public class CreateCarDtoTemplates implements TemplateLoader {

    public static final String COMPLETE_CREATE_CAR_DTO = "complete create car";

    @Override
    public void load() {
        Fixture.of(CreateCarDto.class).addTemplate(COMPLETE_CREATE_CAR_DTO, new Rule() {
            {
                add("year", 1);
                add("licensePlate", "licensePlate");
                add("model", "model");
                add("color", "color");
            }
        });
    }
}
