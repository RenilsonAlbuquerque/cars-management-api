package com.pitang.carsmanager.fixture.user.model;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;

public class CreateUserModelTemplates implements TemplateLoader {

    public static final String COMPLETE_CREATE_USER_MODEL = "complete create user model";

    @Override
    public void load() {
        Fixture.of(CreateUserModel.class).addTemplate(COMPLETE_CREATE_USER_MODEL, new Rule() {
            {
                add("firstName", "firstName");
                add("lastName", "lastName");
                add("email", "email");
                add("login", "login");
                add("password", "password");
                add("birthday", "birthday");
                add("phone", "phone");
            }
        });
    }
}
