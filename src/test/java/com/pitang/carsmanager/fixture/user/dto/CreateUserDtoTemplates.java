package com.pitang.carsmanager.fixture.user.dto;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.pitang.carsmanager.entrypoint.rest.user.dto.CreateCarDto;
import com.pitang.carsmanager.entrypoint.rest.user.dto.CreateUserDto;

import static com.pitang.carsmanager.fixture.car.dto.CreateCarDtoTemplates.COMPLETE_CREATE_CAR_DTO;

public class CreateUserDtoTemplates implements TemplateLoader {

    public static final String COMPLETE_CREATE_USER_DTO = "complete order item";

    @Override
    public void load() {
        Fixture.of(CreateUserDto.class).addTemplate(COMPLETE_CREATE_USER_DTO, new Rule() {
            {
                add("firstName", "firstName");
                add("lastName", "lastName");
                add("email", "email");
                add("login", "login");
                add("password", "password");
                add("birthday", "birthday");
                add("phone", "phone");
                add("cars", has(2).of(CreateCarDto.class,
                        COMPLETE_CREATE_CAR_DTO));
            }
        });
    }
}
