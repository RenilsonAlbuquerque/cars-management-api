package com.pitang.carsmanager.fixture.user.entity;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.pitang.carsmanager.dataproviders.user.entity.UserEntity;

public class UserEntityTemplates implements TemplateLoader {

    public static final String COMPLETE_USER_ENTITY = "complete user entity";

    @Override
    public void load() {
        Fixture.of(UserEntity.class).addTemplate(COMPLETE_USER_ENTITY, new Rule() {
            {
                add("firstName", "firstName");
                add("lastName", "lastName");
                add("email", "email");
                add("login", "login");
                add("userPassword", "password");
                add("birthday", "birthday");
                add("phone", "phone");
            }
        });
    }
}
