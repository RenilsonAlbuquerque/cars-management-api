package com.pitang.carsmanager.entrypoint.rest.user.mapper;

import com.pitang.carsmanager.domain.car.model.CarModel;
import com.pitang.carsmanager.domain.user.model.CreateCarModel;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.model.UserModel;
import com.pitang.carsmanager.entrypoint.rest.user.dto.CreateCarDto;
import com.pitang.carsmanager.entrypoint.rest.user.dto.CreateUserDto;
import org.springframework.stereotype.Component;

@Component
public class CreateUserRestMapper {

    public CreateUserModel dtoToDomainModel(CreateUserDto dto) {
        return CreateUserModel.builder()
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .email(dto.getEmail())
                .birthday(dto.getBirthday())
                .login(dto.getLogin())
                .password(dto.getPassword())
                .phone(dto.getPhone())
                .cars(dto.getCars().stream().map(this::dtoToDomainModel).toList())
                .build();
    }

    private CreateCarModel dtoToDomainModel(CreateCarDto dto) {
        return CreateCarModel.builder()
                .year(dto.getYear())
                .licensePlate(dto.getLicensePlate())
                .color(dto.getColor())
                .model(dto.getModel())
                .build();
    }

    public CreateUserDto domainToDto(UserModel model) {
        return CreateUserDto.builder()
                .firstName(model.getFirstName())
                .lastName(model.getLastName())
                .email(model.getEmail())
                .birthday(model.getBirthday())
                .login(model.getLogin())
                .password(model.getPassword())
                .phone(model.getPhone())
                .cars(model.getCars().stream().map(this::domainModelToDto).toList())
                .build();
    }

    private CreateCarDto domainModelToDto(CarModel model) {
        return CreateCarDto.builder()
                .year(model.getYear())
                .licensePlate(model.getLicensePlate())
                .color(model.getColor())
                .model(model.getModel())
                .build();
    }


}
