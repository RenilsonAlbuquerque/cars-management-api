package com.pitang.carsmanager.entrypoint.rest.user.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserDto {
    private String firstName;
    private String lastName;
    private String email;
    private String birthday;
    private String login;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;
    private String phone;
    private List<CreateCarDto> cars;
}
