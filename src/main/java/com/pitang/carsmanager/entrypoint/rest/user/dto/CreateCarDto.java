package com.pitang.carsmanager.entrypoint.rest.user.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateCarDto {

    private int year;
    private String licensePlate;
    private String model;
    private String color;
}
