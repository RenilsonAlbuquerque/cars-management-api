package com.pitang.carsmanager.entrypoint.rest.user;

import com.pitang.carsmanager.domain.user.contracts.ICreateUserUsecase;
import com.pitang.carsmanager.domain.user.contracts.IFindAllUsersUsecase;
import com.pitang.carsmanager.entrypoint.rest.user.dto.CreateUserDto;
import com.pitang.carsmanager.entrypoint.rest.user.mapper.CreateUserRestMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final CreateUserRestMapper mapper;
    private final ICreateUserUsecase createUserUsecase;
    private final IFindAllUsersUsecase findAllUsersUsecase;

    @PostMapping
    public CreateUserDto createUser(@RequestBody CreateUserDto createUserDto) {
        this.createUserUsecase.execute(
                this.mapper.dtoToDomainModel(createUserDto)
        );
        return createUserDto;
    }

    @GetMapping
    public List<CreateUserDto> listAllUsers() {
        return this.findAllUsersUsecase.execute()
                .stream().map(this.mapper::domainToDto).toList();
    }
}
