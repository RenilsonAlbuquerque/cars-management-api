package com.pitang.carsmanager.entrypoint.rest.commons;

import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.entrypoint.rest.commons.dto.ResponseErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerImpl {

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ResponseErrorDto> handleBusinessException(
            BusinessException businessException) {
        return new ResponseEntity<ResponseErrorDto>(ResponseErrorDto.builder()
                .errorCode(businessException.getCode())
                .message(businessException.getMessage())
                .build(), HttpStatus.BAD_REQUEST);
    }
}
