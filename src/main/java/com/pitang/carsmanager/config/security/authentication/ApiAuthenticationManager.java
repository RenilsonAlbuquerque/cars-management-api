package com.pitang.carsmanager.config.security.authentication;

import com.pitang.carsmanager.config.security.authentication.model.AuthenticationContext;
import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.domain.commons.model.BusinessExceptionEnum;
import com.pitang.carsmanager.domain.user.contracts.IFindUserByLogin;
import com.pitang.carsmanager.domain.user.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ApiAuthenticationManager implements AuthenticationManager {

    private final IFindUserByLogin findUserByLogin;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        Optional<UserModel> user = this.findUserByLogin.execute(username);
        if (user.isPresent() && user.get().getPassword().equals(password)) {
            return new AuthenticationContext(user.get().getId(), user.get().getFirstName());
        }
        else {
            throw new BusinessException(BusinessExceptionEnum.INVALID_LOGIN.code,
                        BusinessExceptionEnum.INVALID_LOGIN.message);
        }
    }
}
