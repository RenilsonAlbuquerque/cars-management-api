package com.pitang.carsmanager.config.security.jwt;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.shaded.gson.Gson;
import com.pitang.carsmanager.config.security.authentication.model.AuthenticationContext;
import com.pitang.carsmanager.config.security.jwt.model.JwtPayload;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;

@Component
public class JwtTokenProvider {


    public String generateToken(AuthenticationContext authenticationContext) {
        try{
            final var payload = JwtPayload.builder()
                    .id(authenticationContext.getId())
                    .name(authenticationContext.getName()).build();

            JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256),
                    new Payload(new Gson().toJson(payload)));

            byte[] sharedKey = new byte[32];
            new SecureRandom().nextBytes(sharedKey);

            jwsObject.sign(new MACSigner(sharedKey));

            return jwsObject.serialize();
        } catch (JOSEException  exception) {
            return "";
        }

    }
}
