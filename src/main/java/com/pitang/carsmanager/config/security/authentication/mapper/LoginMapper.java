package com.pitang.carsmanager.config.security.authentication.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pitang.carsmanager.entrypoint.rest.commons.dto.LoginDTO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.io.IOException;

public class LoginMapper {

    public static UsernamePasswordAuthenticationToken getLoginDtoFromServletRequest(
            HttpServletRequest request) {
        try {
            LoginDTO loginDTO = new ObjectMapper().readValue(request.getInputStream(), LoginDTO.class);
            return new UsernamePasswordAuthenticationToken(loginDTO.getLogin(),loginDTO.getPassword());
        }
        catch (IOException exception) {
            return new UsernamePasswordAuthenticationToken("","");
        }

    }
}
