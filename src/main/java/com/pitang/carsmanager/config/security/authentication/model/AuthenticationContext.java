package com.pitang.carsmanager.config.security.authentication.model;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class AuthenticationContext implements Authentication {
    private Boolean authenticated;
    private Long id;
    private String name;

    public AuthenticationContext(Long id, String name) {
        super();
        this.authenticated = true;
        this.id = id;
        this.name = name;

    }

    public long getId() {
        return this.id;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return this.authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return this.name;
    }
}
