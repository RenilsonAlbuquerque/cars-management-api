package com.pitang.carsmanager.config.security.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.shaded.gson.Gson;
import com.pitang.carsmanager.config.security.authentication.mapper.LoginMapper;
import com.pitang.carsmanager.config.security.authentication.model.AuthenticationContext;
import com.pitang.carsmanager.config.security.jwt.JwtTokenProvider;
import com.pitang.carsmanager.config.security.jwt.model.JwtTokenResponseDto;
import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.domain.commons.model.BusinessExceptionEnum;
import com.pitang.carsmanager.entrypoint.rest.commons.dto.LoginDTO;
import com.pitang.carsmanager.entrypoint.rest.commons.dto.ResponseErrorDto;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.io.IOException;

public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private JwtTokenProvider jwtTokenProvider;
    public AuthenticationFilter(String url, AuthenticationManager authenticationManager,
                                JwtTokenProvider jwtTokenProvider) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
        this.jwtTokenProvider = jwtTokenProvider;
    }
    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request,
            HttpServletResponse response)
            throws AuthenticationException {
        UsernamePasswordAuthenticationToken authRequest = LoginMapper.getLoginDtoFromServletRequest(request);
        try {
            return this.getAuthenticationManager().authenticate(authRequest);
        } catch (BusinessException businessException) {
            throw new BadCredentialsException("Bad credentials");
        }
    }


    @Override
    protected void successfulAuthentication(
            HttpServletRequest req,
            HttpServletResponse res, FilterChain chain,
            Authentication auth) throws IOException {
        AuthenticationContext authContext = (AuthenticationContext) auth;
        final var responseBody =  JwtTokenResponseDto.builder()
                .token(this.jwtTokenProvider.generateToken(authContext))
                .build();
        this.buildResponse(res,HttpStatus.OK.value(),responseBody);
    }

    @Override
    protected void unsuccessfulAuthentication(
            HttpServletRequest req,
            HttpServletResponse res,
            AuthenticationException failed) throws IOException {
        final var responseBody = ResponseErrorDto.builder()
                .errorCode(BusinessExceptionEnum.INVALID_LOGIN.code)
                .message(BusinessExceptionEnum.INVALID_LOGIN.message)
                .build();
        this.buildResponse(res,HttpStatus.UNAUTHORIZED.value(),responseBody);

    }

    private void buildResponse(final HttpServletResponse res, final int httpStatus, final Object body)
            throws IOException {
        String json = new Gson().toJson(body);
        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");
        res.setStatus(httpStatus);
        res.getWriter().println(json);
    }

}
