package com.pitang.carsmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsmanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarsmanagerApplication.class, args);
	}

}
