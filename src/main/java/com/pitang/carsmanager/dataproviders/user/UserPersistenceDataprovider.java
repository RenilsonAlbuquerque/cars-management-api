package com.pitang.carsmanager.dataproviders.user;

import com.pitang.carsmanager.dataproviders.user.mapper.UserRepositoryMapper;
import com.pitang.carsmanager.dataproviders.user.repository.UserRepositoryH2;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserPersistenceDataprovider implements IUserPersistenceGateway {

    private final UserRepositoryMapper mapper;
    private final UserRepositoryH2 userRepository;
    @Override
    public CreateUserModel save(CreateUserModel createUserModel) {
        userRepository.save(this.mapper.domainModelToEntity(createUserModel));
        return createUserModel;
    }

    @Override
    public Optional<UserModel> findByEmail(String email) {
        return this.userRepository.findByEmail(email)
                .map(this.mapper::entityToDomainModel);
    }

    @Override
    public Optional<UserModel> findByLogin(String login) {
        return this.userRepository.findByLogin(login)
                .map(this.mapper::entityToDomainModel);
    }

    @Override
    public List<UserModel> findAll() {
        return this.userRepository.findAll()
                .stream().map(this.mapper::entityToDomainModel)
                .toList();
    }
}
