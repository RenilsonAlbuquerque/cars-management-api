package com.pitang.carsmanager.dataproviders.user.mapper;

import com.pitang.carsmanager.dataproviders.user.entity.UserEntity;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.model.UserModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserRepositoryMapper {

    public UserEntity domainModelToEntity(CreateUserModel model) {
        return UserEntity.builder()
                .firstName(model.getFirstName())
                .lastName(model.getLastName())
                .email(model.getEmail())
                .birthday(model.getBirthday())
                .login(model.getLogin())
                .userPassword(model.getPassword())
                .phone(model.getPhone())
                .build();
    }

    public UserModel entityToDomainModel(UserEntity entity) {
        return UserModel.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .email(entity.getEmail())
                .birthday(entity.getBirthday())
                .login(entity.getLogin())
                .password(entity.getUserPassword())
                .phone(entity.getPhone())
                .cars(List.of())
                .build();
    }
}
