package com.pitang.carsmanager.domain.user.usecase.create.validators;

import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.domain.commons.model.BusinessExceptionEnum;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class InvalidFieldsValidator implements ICreateUserValidator{

    private final Validator validator;
    @Override
    public boolean validate(CreateUserModel createUserModel) {
        Set<ConstraintViolation<CreateUserModel>> violations = validator.validate(createUserModel);
        if(!violations.isEmpty()) {
            for (ConstraintViolation<CreateUserModel> violation : violations) {
                try {
                    Field field = violation.getRootBeanClass().getDeclaredField(violation.getPropertyPath().toString());
                    Annotation annotation = field.getAnnotation(NotNull.class);
                    if (annotation != null) {
                        throw new BusinessException(BusinessExceptionEnum.MISSING_FIELDS.code,
                                BusinessExceptionEnum.MISSING_FIELDS.message);
                    }
                } catch (NoSuchFieldException ignored) {
                    continue;
                }
            }
        }
        return true;
    }
}
