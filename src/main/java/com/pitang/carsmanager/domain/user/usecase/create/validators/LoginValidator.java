package com.pitang.carsmanager.domain.user.usecase.create.validators;

import com.pitang.carsmanager.domain.commons.model.BusinessException;
import com.pitang.carsmanager.domain.commons.model.BusinessExceptionEnum;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoginValidator implements ICreateUserValidator {

    private final IUserPersistenceGateway userPersistenceGateway;
    @Override
    public boolean validate(CreateUserModel createUserModel) {
        final var foundUser = userPersistenceGateway.findByLogin(createUserModel.getLogin());
        if (foundUser.isPresent()) {
            throw new BusinessException(BusinessExceptionEnum.LOGIN_ALREADY_EXISTS.code,
                    BusinessExceptionEnum.LOGIN_ALREADY_EXISTS.message);
        }
        return true;
    }
}
