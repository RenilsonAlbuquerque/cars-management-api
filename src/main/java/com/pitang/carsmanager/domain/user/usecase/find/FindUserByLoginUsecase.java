package com.pitang.carsmanager.domain.user.usecase.find;

import com.pitang.carsmanager.domain.user.contracts.IFindUserByLogin;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FindUserByLoginUsecase implements IFindUserByLogin {

    private final IUserPersistenceGateway userPersistenceGateway;

    public Optional<UserModel> execute(String login) {
        return userPersistenceGateway.findByLogin(login);
    }
}
