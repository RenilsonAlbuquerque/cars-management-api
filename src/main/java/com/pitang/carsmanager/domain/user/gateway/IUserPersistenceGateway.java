package com.pitang.carsmanager.domain.user.gateway;

import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.model.UserModel;

import java.util.List;
import java.util.Optional;

public interface IUserPersistenceGateway {

    CreateUserModel save(CreateUserModel createUserModel);

    Optional<UserModel> findByEmail(String email);

    Optional<UserModel> findByLogin(String login);

    List<UserModel> findAll();
}
