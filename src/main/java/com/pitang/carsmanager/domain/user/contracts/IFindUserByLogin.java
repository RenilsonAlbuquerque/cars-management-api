package com.pitang.carsmanager.domain.user.contracts;

import com.pitang.carsmanager.domain.user.model.UserModel;

import java.util.Optional;

public interface IFindUserByLogin {

    Optional<UserModel> execute(String login);
}
