package com.pitang.carsmanager.domain.user.usecase.create.validators;

import com.pitang.carsmanager.domain.user.model.CreateUserModel;

public interface ICreateUserValidator {

    boolean validate(CreateUserModel createUserModel);
}
