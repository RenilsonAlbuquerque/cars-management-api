package com.pitang.carsmanager.domain.user.contracts;

import com.pitang.carsmanager.domain.user.model.UserModel;

import java.util.List;

public interface IFindAllUsersUsecase {

    List<UserModel> execute();
}
