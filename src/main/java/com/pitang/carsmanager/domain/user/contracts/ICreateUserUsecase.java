package com.pitang.carsmanager.domain.user.contracts;

import com.pitang.carsmanager.domain.user.model.CreateUserModel;

public interface ICreateUserUsecase {

    CreateUserModel execute(CreateUserModel user);
}
