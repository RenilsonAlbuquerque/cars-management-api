package com.pitang.carsmanager.domain.user.usecase.find;

import com.pitang.carsmanager.domain.user.contracts.IFindAllUsersUsecase;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllUsersUsecase implements IFindAllUsersUsecase {

    private final IUserPersistenceGateway persistenceGateway;

    @Override
    public List<UserModel> execute() {
        return this.persistenceGateway.findAll();
    }
}
