package com.pitang.carsmanager.domain.user.usecase.create;

import com.pitang.carsmanager.domain.user.contracts.ICreateUserUsecase;
import com.pitang.carsmanager.domain.user.gateway.IUserPersistenceGateway;
import com.pitang.carsmanager.domain.user.model.CreateUserModel;
import com.pitang.carsmanager.domain.user.usecase.create.validators.ICreateUserValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CreateUserUsecase implements ICreateUserUsecase {

    private final List<ICreateUserValidator> validators;
    private final IUserPersistenceGateway userPersistenceGateway;
    @Override
    public CreateUserModel execute(CreateUserModel user) {
        for (var validator: this.validators) {
            validator.validate(user);
        }
        this.userPersistenceGateway.save(user);
        return user;
    }
}
