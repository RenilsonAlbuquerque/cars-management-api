package com.pitang.carsmanager.domain.commons.model;

public enum BusinessExceptionEnum {

    INVALID_LOGIN(1,"Invalid login or password"),
    EMAIL_ALREADY_EXISTS(2,"Email already exists"),
    LOGIN_ALREADY_EXISTS(3,"Login already exists"),
    INVALID_FIELDS(4,"Invalid fields"),
    MISSING_FIELDS(5,"Missing fields");

    public final int code;
    public final String message;

    BusinessExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
