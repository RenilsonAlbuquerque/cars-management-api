package com.pitang.carsmanager.domain.commons.model;

import lombok.Getter;

@Getter
public class BusinessException extends RuntimeException {

    private final int code;

    public BusinessException(final int code, final String message) {
        super(message);
        this.code = code;
    }
}
