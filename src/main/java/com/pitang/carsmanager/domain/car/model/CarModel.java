package com.pitang.carsmanager.domain.car.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarModel {

    private int year;
    private String licensePlate;
    private String model;
    private String color;
}
