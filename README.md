# Car manager API
Este projeto segue um modelo de arquitetura em 3 camadas: entrypoint, domain e dataproviders; visando isolar a lógica de negócios contida no domain.

## Build and run
Para executar este projeto, é necessário executar o comando:

``
./mvnw spring-boot:run
``


Para executar os testes, é necessário executar o comando:

``
./mvnw spring-boot:run
``


## ESTÓRIAS DE USUÁRIO

0. Configuração:
   1. Criar repositorio do projeto. [Done]
   2. Criar o projeto em maven. [Done]
   3. Criar o projeto angular.[Done] 
   4. Criar a configuração do host na Aws.[Done] 
1. Como um usuário não cadastrado, desejo realizar o cadastro no sistema para que possa utilizar as funcionalidades do mesmo.
   1. Implementar a tela de cadastro no frontend [Done]
   2. Implementar a estrutura básica de pastas do backend. [Done]
   3. Implementar o cadastro de usuário no backend. [Done]
   4. Implementar os validadores de cadastro no backend. [Done]
2. Como um usuário devidamente cadastrado no sistema, desejo efetuar o login para que possa utilizar as funcionalidades do sistema.
   1. Implementar a tela de login no frontend. [Done]
   2. Implementar um interceptador de requests no frontend. [Done]
   3. Implementer um filter de login que cria o JWT no backend. [Done]
   
3. Como um usuário não cadastrado no sistema, desejo listar todos os usuários.
   1. Implementar a tela de listagem no frontend.
   2. Implementar o endpoint de listagem de usuários.

4. Como um usuário não cadastrado no sistema, desejo listar todos os usuários.
   1. Implementar a tela de listagem no frontend.
   2. Implementar o endpoint de listagem de usuários.